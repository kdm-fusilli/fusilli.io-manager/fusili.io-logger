import os

from kafka import KafkaConsumer
import json
import requests
import sys
import logging
from configparser import RawConfigParser
# Get an instance of a logger
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - fusilli.io-logger - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
config = RawConfigParser()
config.read('fusilli_logger.ini')

logger.info('Consumer Logger Starting ...')

try:
    kafka_configuration = json.loads(os.environ['FUSILLI_KAFKA_CONFIGURATION'])
except:
    kafka_configuration = json.loads(config.get('kafka', 'FUSILLI_KAFKA_CONFIGURATION'))

try:
    logger_urls = os.environ['FUSILLI_LOGGER_URLS']
except:
    logger_urls = config.get('fusilli_pipelines_logger', 'FUSILLI_LOGGER_URLS')

kafka_ip = kafka_configuration.get('host', 'localhost')
kafka_port = kafka_configuration.get('port', '9092')
kafka_server = kafka_ip + ':' + str(kafka_port)
logger.debug('Consumer Logger will start with this kafka configuration: ' + str(kafka_server))
logger.debug('Consumer Logger will start with this logger urls configuration: ' + str(logger_urls))
consumer = KafkaConsumer(kafka_configuration.get('topic', 'fusilli_pipelines_logger'),
                         group_id=kafka_configuration.get('group_id', 'group-id'), bootstrap_servers=[kafka_server])
for message in consumer:
    logger_message_original = json.loads(message.value)

    logger_message = {
        "percentage":  logger_message_original.get('percentage', '0'),
        "version":  logger_message_original.get('version', '0.2'),
        "status": logger_message_original.get('status', 'none'),
        "policies_id": logger_message_original.get('policies_id', 'none'),
        "message": logger_message_original.get('message', 'none'),
        "instance_id": int(logger_message_original.get('instance_id', 0)),
        "job_configuration_id": int(logger_message_original.get('job_configuration_id', 0)),
        "timestamp": logger_message_original.get('timestamp', 'none'),
        "pipeline_id": int(logger_message_original.get('pipeline_id', 0))
    }

    pipeline_id = logger_message.get('pipeline_id', 0)
    instance_id = logger_message.get('instance_id', 0)

    if int(pipeline_id) > 0 and int(instance_id) > 0:
        logger.debug('message to log: ' + str(logger_message))
        for logger_url in logger_urls.split('|'):
            data = {"key": "s@g*kjbc^0@07w48)bvvssor*b70s%2bm+b%c6d^dxfj&8)f_*", "log_data": logger_message}
            headers = {'Content-type': 'application/json'}
            response = requests.post(url=logger_url, data=json.dumps(data), headers=headers)
