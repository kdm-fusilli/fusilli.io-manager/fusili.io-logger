import json, requests, sys
import logging

# Get an instance of a logger
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - fusilli.io-logger - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)


message = str("{\"percentage\": 100, \"version\": \"0.2\", \"status\": \"END\", \"policies_id\": \"jhgjhghjghjghj\", \"message\": \"END PIPELINE\", \"instance_id\": 517, \"job_configuration_id\": 0, \"timestamp\": 1608653730888, \"pipeline_id\": 123}")
logger_message = json.loads(message)
pipeline_id = logger_message.get('pipeline_id', 0)
instance_id = logger_message.get('instance_id', 0)
logger_urls = ['http://localhost:8000/fusilli/logger/save-log/']
if int(pipeline_id) > 0 and int(instance_id) > 0:
    logger.debug('message to log: ' + str(logger_message))
    for logger_url in logger_urls:
        data = {"key": "s@g*kjbc^0@07w48)bvvssor*b70s%2bm+b%c6d^dxfj&8)f_*", "log_data": logger_message}
        headers = {'Content-type': 'application/json'}
        response = requests.post(url=logger_url, data=json.dumps(data), headers=headers)
        print(response)